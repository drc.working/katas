export class DiscountCalculator {
  constructor() {
    this.BASE_PRICE = 8
    this.DISCOUNTS = {
      1: 1,
      2: 0.95,
      3: 0.90,
      4: 0.80,
      5: 0.75,
    }
  }

  price(basket) {
    let firstPrice = 0
    let secondPrice = 0

    const MAX_SIZE = 4

    if (basket.length === 0) { return firstPrice }

    const bigSagas = this._buildUniqueSagas(basket)
    const smallSagas = this._buildUniqueSagas(basket, MAX_SIZE)

    bigSagas.forEach(saga => {
      firstPrice +=  this.BASE_PRICE * saga.length * this.DISCOUNTS[saga.length]
    })

    smallSagas.forEach(saga => {
      secondPrice +=  this.BASE_PRICE * saga.length * this.DISCOUNTS[saga.length]
    })

    return Math.min(firstPrice, secondPrice)
  }

  _buildUniqueSagas(basket, maxSize = 5) {
    const sagas = [[]]

    basket.map(book => {
      const saga = sagas.find(saga => { return !saga.includes(book) && saga.length !== maxSize })

      const foundSaga = sagas.indexOf(saga)

      if (foundSaga !== -1) { sagas[foundSaga].push(book) }
      else { sagas.push(new Array(book)) }
    })

    return sagas
  }
}