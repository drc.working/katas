import { expect } from "chai"
import { Bowling } from "../../src/bowling/Bowling.js"
import { Round } from "../../src/bowling/Round.js"

describe('Bowling Game', () => {
  context('Playing 3 rounds', () => {
    it('Instances a object for each round ', () => {
      const rounds = 3
      const game = new Bowling(rounds)
  
      const result = game.rounds
  
      expect(result).to.have.lengthOf(rounds)
    })
  })

  context('Given the rounds and the shots', () => {
    it('Instances the shots within the rounds', () => {
      const shots = [
        {
          firstShot: 8,
          secondShot: 1
        },
        {
          firstShot: 3,
          secondShot: 3
        },
      ]
      const game = new Bowling(shots.length, shots)
  
      const result = game.rounds
  
      expect(result[0]).to.include(shots[0])
      expect(result[1]).to.include(shots[1])
    }) 
  })

  context('Given special rounds', () => {
    it('calculates the score of a single strike round', () => {
      const shots = [
        {
          firstShot: 10,
          secondShot: 0
        }
      ]
      const game = new Bowling(shots.length, shots)
  
      const result = game.rounds
      
      expect(game.score).to.eq(10)
      expect(result[0].score).to.eq(10)
    })

    it('calculates the score of a strike', () => {
      const shots = [
        {
          firstShot: 10,
          secondShot: 0
        },
        {
          firstShot: 2,
          secondShot: 2
        },
        {
          firstShot: 2,
          secondShot: 2
        },
      ]
      const game = new Bowling(shots.length, shots)
  
      const result = game.rounds
      
      expect(game.score).to.eq(26)
      expect(result[0].score).to.eq(18)
      expect(result[1].score).to.eq(4)
    })

    it('calculates the score of a spare', () => {
      const shots = [
        {
          firstShot: 4,
          secondShot: 6
        },
        {
          firstShot: 2,
          secondShot: 2
        },
        {
          firstShot: 2,
          secondShot: 2
        },
      ]
      const game = new Bowling(shots.length, shots)
  
      const result = game.rounds
      
      expect(game.score).to.eq(22)
      expect(result[0].score).to.eq(14)
      expect(result[1].score).to.eq(4)
    })

    it('calculates the score of a strike and spare', () => {
      const shots = [
        {
          firstShot: 10,
          secondShot: 0
        },
        {
          firstShot: 5,
          secondShot: 5
        },
        {
          firstShot: 2,
          secondShot: 2
        },
        {
          firstShot: 2,
          secondShot: 2
        },
      ]
      const game = new Bowling(shots.length, shots)
  
      const result = game.rounds
      
      expect(game.score).to.eq(46)
      expect(result[0].score).to.eq(24)
      expect(result[1].score).to.eq(14)
    })
  })

  context('Given no rounds', () => {
    it('plays random scores', () => {
      const rounds = 9
      const game = new Bowling(rounds)
  
      expect(game.rounds).to.have.lengthOf(9)
    })
  })

  context('Playing final round', () => {
    it('allow play another two rounds when strike is scored', () => {
      const shots = Array.from({length: 1}).map(() => new Round(10, 0))

      const game = new Bowling(shots.length, shots)

      expect(game.rounds).to.have.lengthOf(3)
    })
  })

  context('Playing 10 strike rounds', () => {
    it('scores 300 points', () => {
      const shots = Array.from({length: 10}).map(() => new Round(10, 0))
      
      const game = new Bowling(shots.length, shots)

      expect(game.score).to.eq(300)
    })
  })
})
