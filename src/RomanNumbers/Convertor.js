export class Convertor {
  constructor() {
    this.relations = {
      1: 'I',
      5: 'V',
      10: 'X',
      50: 'L',
      100: 'C',
      500: 'D',
      1000: 'M'
    }

    this.toRomanResult = ''
  }

  resetRomanResult() {
    this.toRomanResult = ''
  }

  toRoman(arabicNumber) {
    return this._recursiveToRoman(arabicNumber)
  }

  _recursiveToRoman(arabicNumber) {
    this.remaining = arabicNumber

    if (this._isNeedToSubstract(this.remaining)) {
      this._substract(this.remaining)
      this._recursiveToRoman(this.remaining)

    } else { this._sum(this.remaining) }

    if (this.remaining === 0) { return this.toRomanResult }
  }

  _sum(arabicNumber) {
    const smallest = this._findSmallestNumber(arabicNumber)
    const romanNumber = this.relations[smallest]

    this.toRomanResult += romanNumber
    this.remaining -= smallest

    if (this.remaining > 0) { this._recursiveToRoman(this.remaining) }

    return this.toRomanResult
  }

  _substract(arabicNumber) {
    const nextBiggestNumber = this._nextBiggestNumber(arabicNumber)
    const nextSmallestTenthPower = this._nextSmallestTenthPower(arabicNumber)

    this.remaining -= nextBiggestNumber - nextSmallestTenthPower
    this.toRomanResult += this.relations[nextSmallestTenthPower] + this.relations[nextBiggestNumber]
  }

  _nextBiggestNumber(arabicNumber) {
    const arabicNumbers = Object.keys(this.relations)

    return arabicNumbers.find( number => number >= arabicNumber)
  }

  _nextSmallestTenthPower(arabicNumber) {
    const tenthPowers = Object.keys(this.relations).filter(number => Math.log10(number) % 1 === 0)

    return tenthPowers.reverse().find( number => number < arabicNumber)
  }

  _isNeedToSubstract(arabicNumber) {
    const nextSmallestTenthPower = this._nextSmallestTenthPower(arabicNumber)

    const nextBiggestNumber = this._nextBiggestNumber(arabicNumber)

    return (nextBiggestNumber - nextSmallestTenthPower) <= arabicNumber
  }


  _findSmallestNumber(arabicNumber) {
    const arabicNumbers = Object.keys(this.relations)

    const smallestNumber = arabicNumbers.reverse().find( key => key <= arabicNumber )

    return smallestNumber
  }
}