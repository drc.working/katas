import { expect } from 'chai'
import { Convertor } from '../../src/RomanNumbers/Convertor.js'
const convertor = new Convertor()

describe('Roman Number', () => {
  beforeEach(() => {
    convertor.resetRomanResult()
  })
  
  context('Given arabic numbers', () => {
    it('converts the 1', () => {
      const arabic = 1
      const expectedResult = 'I'
      
      const result = convertor.toRoman(arabic)

      expect(result).to.deep.equal(expectedResult)
    })

    it('converts the 3', () => {
      const arabic = 3
      const expectedResult = 'III'
      
      convertor.toRoman(arabic)
      
      const result = convertor.toRomanResult
      expect(result).to.deep.equal(expectedResult)
    })

    it('converts the 8', () => {
      const arabic = 8
      const expectedResult = 'VIII'
      
      convertor.toRoman(arabic)
      
      const result = convertor.toRomanResult
      expect(result).to.deep.equal(expectedResult)
    })

    it('converts the 4', () => {
      const arabic = 4
      const expectedResult = 'IV'
      
      convertor.toRoman(arabic)
      
      const result = convertor.toRomanResult
      expect(result).to.deep.equal(expectedResult)
    })

    it('converts the 9', () => {
      const arabic = 9
      const expectedResult = 'IX'
      
      convertor.toRoman(arabic)
      
      const result = convertor.toRomanResult
      expect(result).to.deep.equal(expectedResult)
    })

    it('converts the 44', () => {
      const arabic = 44
      const expectedResult = 'XLIV'
      
      convertor.toRoman(arabic)
      
      const result = convertor.toRomanResult
      expect(result).to.deep.equal(expectedResult)
    })

    it('converts the 49', () => {
      const arabic = 49
      const expectedResult = 'XLIX'
      
      convertor.toRoman(arabic)
      
      const result = convertor.toRomanResult
      expect(result).to.deep.equal(expectedResult)
    })

    it('converts the 99', () => {
      const arabic = 99
      const expectedResult = 'XCIX'
      
      convertor.toRoman(arabic)
      
      const result = convertor.toRomanResult
      expect(result).to.deep.equal(expectedResult)
    })

    it('converts the 437', () => {
      const arabic = 437
      const expectedResult = 'CDXXXVII'
      
      convertor.toRoman(arabic)
      
      const result = convertor.toRomanResult
      expect(result).to.deep.equal(expectedResult)
    })
  })
})