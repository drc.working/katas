import { Round } from "./Round.js"

export class Bowling {
  constructor(numberOfRounds, shots = false) {
    this.rounds = Array.from({length: numberOfRounds}).map(() => new Round())
    this._setShots(shots)
    this.score = this._calculateRoundsScores()
  }

  _calculateRoundsScores() {
    this.score = 0
    this.rounds.map(round => round.calculateScore())
    
    this.rounds.map((round, index) => {
      round.calculateScore()
      
      if (this._isStrike(round)) {
        round.score = 10 + this.rounds[index + 1]?.score + this.rounds[index + 2]?.score

        if (isNaN(round.score)) { round.score = 10 }

      } else if (this._isSpare(round)) {
        round.score = 10 + this.rounds[index + 1]?.score
        
        if (isNaN(round.score)) { round.score = 10 }
      }
    })

    this.rounds.forEach((round) => {this.score += round.score})
    // this.rounds.forEach((round, index) => {console.log(index + 1, round)})
    // console.log('Total Score', this.score)
    return this.score
  }

  _randomScore(cap) {
    return Math.round(Math.random() * cap)
  }

  _playRandom() {
    this.rounds.map((round) => {
      round.firstShot = this._randomScore(10)
      round.secondShot = this._randomScore(10 - round.firstShot)
    })
  }

  _setShots(shots) {
    if (!shots) {
      this._playRandom()
      return 
    }

    this.rounds.map((round, index) => {
      round.firstShot = shots[index].firstShot
      round.secondShot = shots[index].secondShot
    })
  }

  _isStrike(round) {
    return round.firstShot === 10 && round.secondShot === 0
  }

  _isSpare(round) {
    return round.firstShot + round.secondShot === 10
  }
}
