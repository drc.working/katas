export class Round {
  constructor(firstShot = 0, secondShot = 0) {
    this.firstShot = firstShot
    this.secondShot = secondShot
    this.score = 0
  }

  calculateScore() {
    this.score = this.firstShot + this.secondShot
  }
}
