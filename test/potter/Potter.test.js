import { expect } from 'chai'
import { DiscountCalculator } from '../../src/potter/Potter.js'

const calculator = new DiscountCalculator()
const DISCOUNTS = {
  1: 1,
  2: 0.95,
  3: 0.90,
  4: 0.80,
  5: 0.75,
}
const BASE_PRICE = 8

describe('Potter', () => {
  context('Given baskets with books', () => {
    context('generates unique sagas', () => {
      it('from same books', () => {
        const basket = ['h1', 'h1']
        const expectedResult = [['h1'], ['h1']]

        const result = calculator._buildUniqueSagas(basket)

        expect(result).to.deep.equal(expectedResult)
      })

      it('from different books', () => {
        const basket = ['h1', 'h1', 'h2']
        const expectedResult = [['h1', 'h2'], ['h1']]

        const result = calculator._buildUniqueSagas(basket)

        expect(result).to.deep.equal(expectedResult)
      })

      it('with specific length', () => {
        const basket = ['h1', 'h2', 'h3', 'h4', 'h5']
        const expectedResult = [['h1', 'h2', 'h3', 'h4'], ['h5']]

        const result = calculator._buildUniqueSagas(basket, 4)

        expect(result).to.deep.equal(expectedResult)
      })
    })

    it('calculates the price for empty basket', () => {
      const basket = []
      const expectedResult = 0

      const result = calculator.price(basket)

      expect(result).to.deep.equal(expectedResult)
    })

    it('calculates the price for same books', () => {
      const basket = ['h1', 'h1']
      const expectedResult = 16

      const result = calculator.price(basket)

      expect(result).to.deep.equal(expectedResult)
    })

    it('calculates the price for 2 different books', () => {
      const basket = ['h1', 'h2']
      const expectedPrice = BASE_PRICE * basket.length * DISCOUNTS[basket.length]

      const result = calculator.price(basket)

      expect(result).to.deep.equal(expectedPrice)
    })

    it('calculates the price for several discounts', () => {
      const basket = ['h1', 'h2', 'h1', 'h2']
      const expectedPrice = 2 * BASE_PRICE * 2 * DISCOUNTS[2]

      const result = calculator.price(basket)

      expect(result).to.deep.equal(expectedPrice)
    })
    context('Edge cases', () => {
      it('calculates the price for a basket with 8 books and different sagas', () => {
        const basket = ['h1', 'h2', 'h2', 'h1', 'h3', 'h3', 'h5', 'h4']
        const expectedResult = 2 * (8 * 4 * DISCOUNTS[4])

        const result = calculator.price(basket)

        expect(result).to.deep.equal(expectedResult)
      })

      it('calculates the price for a basket with 12 books and different sagas', () => {
        const basket = ['h1', 'h2', 'h3', 'h4', 'h5', 'h2', 'h1', 'h3', 'h4', 'h5', 'h1', 'h2']
        const expectedResult = 2 * (8 * 5 * DISCOUNTS[5]) + (8 * 2 * DISCOUNTS[2])

        const result = calculator.price(basket)

        expect(result).to.deep.equal(expectedResult)
      })
    })
  })
})